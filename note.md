
    message
        - id : int
        - slug: string          # slugified title
        - messageid : string    # id messaggio da headers
        - mailinglist: list_id
        - subject : string
        - sender : user_id
        - date : datetime
        - body : text
        - eml : string          # raw mail message
        - parent: message_id    # in reply to

    list:
        - id : int
        - slug: string
        - listid : string
        - post_url: string      # da header "List-Post"
        - help_url: string      # da header "List-Help"
        - sub_url: string       # da header "List-Subscribe"
    
    user:
        - id : int
        - email: string
        - name : string

    tag:
        - id: int
        - name: string

## links

https://charlesleifer.com/blog/using-sqlite-full-text-search-with-python/

https://charlesleifer.com/blog/querying-tree-structures-in-sqlite-using-python-and-the-transitive-closure-extension/

http://docs.peewee-orm.com/en/latest/

https://github.com/coleifer/ucache # puo' essere utile?


## routes

    /                 : (index) list of threads from all mls
    /r/<slug>         : (mailinglist) list of thread from list "slug"
    /m/<id>[/<slug>]  : (message) thread view
    /s?q=             : (search)