"""
Send email

config:

SMTP_HOST
SMTP_PORT       (default 25)
SMTP_USER
SMTP_PASSWORD
SMTP_SSL = "no" (default), "ssl", "startssl"

used in view to compose mail
EMAIL_FROM      (default "liar")
EMAIL_SUBJECT   (default "LiAr - login")

"""

import smtplib, ssl, os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from .app import app

__all__ = ['send_mail', 'SmtpStartSSLException']


class SmtpStartSSLException(Exception):
     ...


def send_mail(from_addr, to_addr, subj, message_text, message_html = None ):
     msg = msg_txt = MIMEText(message_text)

     if message_html is not None:
          msg_html = MIMEText(message_html, 'html')
          msg = MIMEMultipart()
          msg.attach(msg_txt)
          msg.attach(msg_html)

     msg['Subject'] = subj
     msg['From'] = from_addr
     msg['To'] = to_addr

     smtp_server = create_smtp_server()
     smtp_server.sendmail(from_addr, to_addr, msg.as_string())
     smtp_server.quit()


def create_smtp_server():
     smtp = app.config.get_namespace("SMTP_")

     _DEFAULT_CIPHERS = (
     'ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+HIGH:'
     'DH+HIGH:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+HIGH:RSA+3DES:!aNULL:'
     '!eNULL:!MD5')

     usessl = smtp.get("ssl", "no").lower()

     _host = smtp.get('host', 'localhost')
     _port = smpt.get('port', 25)

     if usessl == "ssl":
          smtp_server = smtplib.SMTP_SSL(_host, _port)
     else:
          smtp_server = smtplib.SMTP(_host, _port)

     if usessl == "starttls" or usessl == "ssl":
          # only TLSv1 or higher
          context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
          context.options |= ssl.OP_NO_SSLv2
          context.options |= ssl.OP_NO_SSLv3

          context.set_ciphers(_DEFAULT_CIPHERS)
          context.set_default_verify_paths()
          context.verify_mode = ssl.CERT_REQUIRED

     if usessl == "startssl":
          if smtp_server.starttls(context=context)[0] != 220:
               raise SmtpStartSSLException()

     smtp_server.login(smtp['user'], smtp['password'])
     return smtp_server

