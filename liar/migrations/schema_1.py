from playhouse.migrate import *

from ..models import User

def up(migrator):
    migrate(
        migrator.add_column('user', 'is_admin', User.is_admin),
    )
