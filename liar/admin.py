from flask_admin.form import SecureForm
from flask_admin import Admin
from flask_admin.contrib.peewee import ModelView
from flask import (
    g, redirect, url_for, request
)

from .app import app
from .models import User, MailingList, Message, Tag

class BaseModelView(ModelView):
    form_base_class = SecureForm
    def is_accessible(self):
        if g.get('user') is None:
            return False
        return g.get('user').is_admin

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('login', next=request.url))

class UserAdmin(BaseModelView):
    pass

class MLAdmin(BaseModelView):
    pass

class MessageAdmin(BaseModelView):
    pass

class TagAdmin(BaseModelView):
    pass



admin = Admin(app, name='LiAr') #, template_mode='bootstrap3')
admin.add_view(UserAdmin(User))
admin.add_view(MLAdmin(MailingList))
admin.add_view(MessageAdmin(Message))
admin.add_view(TagAdmin(Tag))