## ingest mail from stdinput or pipermail .txt.gz
import peewee
import re
import sys
import mailparser
import email
import quopri
import gzip

from ..models import User, MailingList, Message, Tag
from ..utils import strip_tags

CODECS = ['ascii', 'utf_8', 'iso8859_15']

def force_decode(string, codecs=CODECS):
    for i in codecs:
        try:
            return string.decode(i)
        except UnicodeDecodeError:
            pass


# _char_re = re.compile(b'=[B-F0-9]{2}')
# def _decode(m):
#     return bytes([int(m.group(0).strip(b'='),16)])

# def decode(bytestr):
#     return _char_re.sub(_decode, bytestr).decode("ISO-8859-15")

def decode_quoted(string):
    return force_decode(quopri.decodestring(string))

def ingest(mailstr, mlist):
    # read and parse

    #mail = mailparser.parse_from_string(mailstr)
    mail = mailparser.parse_from_bytes(mailstr.encode("utf_8"))

    print("Importing message id '{}'".format(mail.message_id))

    if mail.message_id == "":
        return "Ignoring message without ID"

    # don't save already parsed message
    q = Message.select().where(Message.messageid == mail.message_id)
    if q.count() > 0:
        ## update index
        q.get().ftsindex()
        return "Message with id '{}' already parsed".format(mail.message_id)

    # sender
    try:
        s_name, s_email = mail.from_[0]
    except IndexError as e:
        print(mailstr)
        raise e

    sender, isnew = User.get_or_create(email = s_email)
    if isnew:
        sender.name = s_name
        sender.save()

    # parent (we use both in_reply_to and references)
    in_reply_to = mail.in_reply_to + mail.references
    for s in " \r\n\t":
        in_reply_to = in_reply_to.replace(s, "") 
    
    parent = None
    # if in_reply_to != "":
    #     #TODO: what if we don't yet have the parent message !?!
    #     # . the parent id could be saved as string in message
    #     # . the parent will be none
    #     # . everytime we import a new message, we search for messages with (parent_id == newmessage_id and parent == None)

    #     rids = [ "<{}>".format(rid) for rid in re.split("><", in_reply_to.strip("<>")) ]
       
    #     try:
    #         parent = Message.get(Message.messageid << rids) #get the first one if there 
    #     except Message.DoesNotExist:
    #         print("\tParent message in {} does not exists".format(rids))
 
    subject, tags = strip_tags(mail.subject)

    # tags
    # skip ml slug tags
    mlslugs = MailingList.select( MailingList.slug ).scalar(as_tuple=True)

    tagslist = []
    for t in tags:
        t = t.lower()
        if t in mlslugs:
            continue
        tag, isnew = Tag.get_or_create(name = t)
        if isnew:
            tag.save()
        tagslist.append(tag)
    tagslist = list(set(tagslist))


    try:
        body = mail.text_plain[0], ## !!! diamo per scontato che il primo elemento solo-testo sia il corpo principale della mail!
    except IndexError:
        body = mail.body

    if not isinstance(body, str):
        body = '\n\n'.join(body)
    
    try:
        #body = decode_quote(body)
        pass
    except UnicodeEncodeError:
        pass

    if "This is a multi-part message in MIME format.\n" in body:
        print("\tis multipart")
        sep = body.splitlines()[1][2:]
        body = "Content-Type: multipart/mixed; boundary={}\r\n\r\n\r\n{}".format(sep, body)
        body = re.sub(r'\n\n>From -.*', '', body).strip()
        msg = email.message_from_string(body)
        res = []
        for m in msg.walk():
            p = m.get_payload()
            if m.get_content_type() == "text/plain":
                print (m.get_content_type())
                res.append(p)
        body = "\n_____________________________\n\n\n".join(res)


    try:
        body = decode_quoted(body)
    except ValueError as e:
        pass

    message = Message(
        messageid = mail.message_id,
        subject = subject,
        date = mail.date,
        body = body,
        eml = mailstr,
        mailinglist = mlist,
        sender = sender,
        in_reply_to = in_reply_to or None,
        parent = parent
    )
    message.save()

    try:
        message.tags.add(tagslist)
    except peewee.IntegrityError as e:
        print(tagslist)
        raise e

    return None


def fix_parents():
    """find messages with "in_reply_to" not null and parent null and
        look for parent in messages"""
    print ("Fixing oprhans...")
    orphans = Message.select().where( Message.in_reply_to.is_null(False) &  Message.parent.is_null(True) )
    tot = orphans.count()
    fixd = 0
    for message in orphans:
        rids = [ "<{}>".format(rid) for rid in re.split("><", message.in_reply_to.strip("<>")) ]
        for rid in rids:
            try:    
                parent = Message.get(Message.messageid == rid )
                message.parent = parent
                message.save()
                fixd += 1
                print("... {} of {}".format(fixd, tot), end="\r")
                break
            except Message.DoesNotExist:
                pass
    print("... {} of {}".format(fixd, tot))

def run():
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        print ("liar ingest - ingest mail from stdin or mbox file (plain or gzipped)")
        print ()
        print ("    liar ingest <list id|list slug> [mbox file]")
        print ()
        sys.exit()

    mlist_id = sys.argv[2]
    try:
        if mlist_id.isnumeric():
            mlist = MailingList.get(int(mlist_id))
        else:
            mlist = MailingList.get( MailingList.slug == mlist_id )
    except MailingList.DoesNotExist:
        sys.exit("Mailing list '{}' does not exists".format(mlist_id))

    if len(sys.argv) == 4:
        inputfile = sys.argv[3]
        print("# import from mbox file {}".format(inputfile))
        
        _o = open
        if (inputfile.endswith(".gz")):
            _o = gzip.open
        
        skip = False
        mailstr = ""
        count = 0
        with _o(inputfile,'rb') as f:
            for l in f.readlines():
                l = force_decode(l)
                if l.strip() == "-------------- parte successiva --------------":
                    skip = True
                if l.startswith("From "):
                    if mailstr != "":
                        r = ingest(mailstr, mlist)
                        if r is not None:
                            print(r)
                        mailstr = ""
                        skip = False
                elif skip == False:
                    mailstr += l
        print("# success! {} messages".format(count))
        #fix_parents()

    else:
        mailstr = sys.stdin.read()
        r = ingest(mailstr, mlist)
        fix_parents()
        if r is not None:
            sys.exit(r)