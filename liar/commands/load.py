"""
    Load fixtures
"""

from ..models import *

# fixture
# TODO: load from file

def run():
    m = MailingList(
        slug = "gl-como",
        listid = "Gruppo Linux Como <gl-como.lists.linux.it>",
        post_url = "mailto:gl-como@lists.linux.it",
        help_url = "mailto:gl-como-request@lists.linux.it?subject=help",
        sub_url = "https://lists.linux.it/listinfo/gl-como"
    )
    m.save()
