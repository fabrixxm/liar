"""
    migrate schema

    load current schema version from "schema.version" in db,
    looks for next schema version as module "..migrations.schema_<n>",
    when module is not found, stops and save new schema version

    schema migration module must implement a `up(migrator)` function, where 
    migration is performed, which get as parameter the migrator object

    eg `schema_1.py`:

    ```
    from playhouse.migrate import *

    def up(migrator):
        migrate(
            migrator.add_column('some_table', 'title', title_field),
        )
    ```

    see
    http://docs.peewee-orm.com/en/latest/peewee/playhouse.html#migrate
"""
from importlib import import_module
from playhouse.migrate import *

from ..models import *

def run():

    run_migrate = True
    if not db.database.table_exists("schema"):
        run_migrate = False
        db.database.create_tables([
            Schema,
            User, 
            MailingList, 
            Message, 
            Tag, 
            MessageTag, 
            FTSMessage
        ])
        return
    
    migrator = SqliteMigrator(db.database)

    current_schema_version = Schema.get().version
    print("Current schema version: {}".format(current_schema_version))
    while True:
        current_schema_version += 1
        module_name = "..migrations.schema_{}".format(current_schema_version)
        try:
            migration = import_module(module_name, __package__)
            if run_migrate:
                print("Migrate to schema version #{}".format(current_schema_version))
                migration.up(migrator)
        except ModuleNotFoundError:
            current_schema_version -= 1
            break
    
    Schema.set(version = current_schema_version)