import re
import os
from unicodedata import normalize

from avatar_generator import Avatar
from PIL import ImageFont



_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

def slugify(text, delim='-'):
    """Generates an slightly worse ASCII-only slug."""
    result = []
    for word in _punct_re.split(text.lower()):
        word = normalize('NFKD', word).encode('ascii', 'ignore')
        if word:
            result.append(word.decode("utf8"))
    return delim.join(result)



_sqrtag_re = re.compile(r'\s?\[([^][]+)\]\s?')

def strip_tags(text):
    """Strip square-brackets tags from subject"""
    tags = _sqrtag_re.findall(text)
    text = _sqrtag_re.sub("", text).strip()
    if text.startswith("[") and text.endswith("]"):
        text = text.strip("[]")

    tags = [ t.strip() for t in tags ]
    return (text, tags)


class CustomFontAvatar(Avatar):
    @staticmethod
    def _font(size):
        """
            Returns a PIL ImageFont instance.
            :param size: size of the avatar, in pixels
        """
        if size < 32:
            path = os.path.join(os.path.dirname(__file__), 'static', 'fonts', "Inconsolata-Bold.ttf")
        else:
            path = os.path.join(os.path.dirname(__file__), 'static', 'fonts', "Inconsolata-Regular.ttf")
        return ImageFont.truetype(path, size=int(0.8 * size))