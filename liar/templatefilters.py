import re
import markdown as md
from bs4 import BeautifulSoup, NavigableString
import uuid

from flask import request

from .app import app

_urlfinder_re = re.compile('(http[s]?:\/\/\S+)', re.MULTILINE)
_bqend_re = re.compile('(>.*\n)([^>\n])', re.MULTILINE)

@app.template_filter()
def markdown(text):
    # add a newline after blockquotes
    text = _bqend_re.sub("\\1\n\\2", text)

    # link naked url
    text = _urlfinder_re.sub(r'<\1>', text)

    text = md.markdown(text, extensions=['nl2br'])

    # remove empty blockquotes
    text = text.replace("<blockquote></blockquote>", "")
    return text


@app.template_filter()
def collapsequote(text):
    soup = BeautifulSoup(text, 'html.parser')

    bqs = soup.find_all('blockquote')
    for bq in bqs:
        bqid = str(uuid.uuid4())
        p = bq.previous_sibling

        while p == '\n':
            p = p.previous_sibling

        if p is not None and p.get_text().strip().endswith(":"):
            if p.find('br') and p.contents[-2].name == "br":
                p.contents[-2].decompose()
                ptext = p.contents[-1].extract()
                newp = soup.new_tag("p")
                newp.append(ptext)
                p.insert_after( newp )
                p = newp

            p['class'] = "quote-pre"

        # add show/hide links before and after the blockquote
        ashow = soup.new_tag('a', **{
            'href':"#",
            'class':'quote-switcher quote-show is-hidden',
            'data-ref':bqid
        })
        ashow.append('show quote')

        ahide = soup.new_tag('a', **{
            'href':"#",
            'class':'quote-switcher quote-hide is-hidden',
            'data-ref':bqid
        })
        ahide.append('hide quote')

        bq.insert_before(ashow)
        bq.insert(0, ahide)
        bq['id'] = bqid

    text = str(soup)

    return text


@app.template_filter()
def mailsig(text):
    soup = BeautifulSoup(text.strip(), 'html.parser')

    issign = False
    p = soup.contents[-1]
    print(p, len(p.contents))
    if p.find('br') and len(p.contents) > 3 and p.contents[-4].name == "br":
        p.contents[-4].decompose()
        newp = soup.new_tag("p")
        for z in range(-3,0):
            elm = p.contents[z].extract()
            if re.match(r'^\n?-- ?',str(elm)):
                issign = True
            print("{!r}".format(str(elm)))
            newp.append(elm)
        p.insert_after( newp )
        p = newp
    else:
        if re.match(r'^\n?-- ?',str(p.contents[0])):
            issign = True

    if issign:
        p['class'] = 'mail-signature'

    text = str(soup)

    return text


"""
_sig_re = re.compile(r'(<br ?/?>|<p>)?[\n\r]*-- ?<br ?/?>.*?</p>$', re.DOTALL)

@app.template_filter()
def mailsig(text):
    p = _sig_re.search(text.strip())
    print(p)
    if p is not None and "</blockquote>" not in p.group(0):
        text = text.replace(p.group(0), p.group(0).replace("<p", "<p class='mail-signature'"))

    return text
"""

_mail_re = re.compile(r"([a-zA-Z0-9_.+-]+@)[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+", re.MULTILINE)
@app.template_filter()
def mailcrop(text):
    return _mail_re.sub("\\1...", text)


@app.template_filter('pluralize')
def pluralize(number, singular = '', plural = 's'):
    if number == 1:
        return singular
    else:
        return plural




class PageURLS:
    def __init__(self, paginatedquery):
        self.pq = paginatedquery

    def page(self, page):
        qs = "page={}".format(page)
        if "page=" in request.full_path:
            return re.sub(r'page=\d+', qs, request.full_path)
        if "?" in request.full_path:
            return request.full_path + "&" + qs
        return request.full_path + "?" + qs

    def prev(self):
        return self.page(self.pq.get_page() - 1)

    def next(self):
        return self.page(self.pq.get_page() + 1)

    def first(self):
        return self.page(1)

    def last(self):
        return self.page(self.pq.get_page_count())


@app.template_filter()
def pageurl(paginatedquery):
    return PageURLS(paginatedquery)
