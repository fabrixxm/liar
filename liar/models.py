import datetime
from peewee import *
from playhouse.flask_utils import FlaskDB
from playhouse.sqlite_ext import FTSModel

from .app import app
from .utils import slugify

db = FlaskDB(app)

class Schema(db.Model):
    version = IntegerField(default=0)

    @classmethod
    def get(cls):
        obj, isnew = cls.get_or_create(id = 1)
        if isnew:
            obj.save()
        return obj
    
    @classmethod
    def set(cls, **kwargs):
        cls.get().update(**kwargs).execute()


class User(db.Model):
    name = CharField(default="")
    email = CharField(index=True)
    is_admin = BooleanField(default=False)

    class Meta:
        database = db.database

    def get_mailinglists(self):
        return self.messages.group_by( Message.mailinglist )

    def get_threads(self):
        return self.messages.where( Message.parent.is_null() ).order_by(-Message.date)


    def __str__(self):
        return "{} <{}>".format(self.name, self.email)


class MailingList(db.Model):
    slug = CharField(index=True)
    listid = CharField()
    post_url = CharField(null=True)
    help_url = CharField(null=True)
    sub_url = CharField(null=True)

    def get_threads(self):
        """return top-level messages for this mailinglist"""
        return self.messages.where( Message.parent.is_null() ).order_by(-Message.date)

    class Meta:
        database = db.database

    def __str__(self):
        return self.listid


class Tag(db.Model):
    name = CharField(index=True)
    color = CharField(default="#00d1b2")

    class Meta:
        database = db.database

    def __str__(self):
        return self.name


class Message(db.Model):
    messageid = CharField(index=True)
    slug = CharField(index=True)
    subject = CharField()
    date = DateTimeField(default=datetime.datetime.now)
    body = TextField()

    in_reply_to = CharField(null=True)

    mailinglist = ForeignKeyField(MailingList, backref='messages')
    sender = ForeignKeyField(User, backref='messages')

    tags = ManyToManyField(Tag, backref='messages')


    parent = ForeignKeyField('self', null=True, backref='children')

    eml = TextField()

    @classmethod
    def get_threads(cls):
        """return all top-level messages"""
        return Message.select().where( Message.parent == None ).order_by(-Message.date)

    @classmethod
    def ftsearch(cls, text):
        """perform full-text search"""
        return (Message
                    .select()
                    .join(FTSMessage, on=(Message.id == FTSMessage.docid))
                    .where(FTSMessage.match(text)))
        


    def get_top_level(self):
        """return top-level parent for this message"""
        message = self
        while message.parent != None:
            message = message.parent
        return message

    def ordered_children(self):
        return self.children.order_by(Message.date)

    def count_children(self):
        thischildren = 0
        for c in self.children:
            thischildren += 1
            thischildren += c.count_children()
        
        return thischildren

    class Meta:
        database = db.database

    def save(self, *args, **kwargs):
        if self.slug is None:
            self.slug = slugify(self.subject)

        super().save(*args, **kwargs)

        self.ftsindex()

    def ftsindex(self):
        o, c = FTSMessage.get_or_create(
            docid=self.id,
            content='\n'.join((self.subject, self.body)))

    def __str__(self):
        return self.subject


class FTSMessage(FTSModel):
    """Full text support for messages"""
    content = TextField()

    class Meta:
        database = db.database


MessageTag = Message.tags.get_through_model()



## TODO: add support for transitive closure
##      https://charlesleifer.com/blog/querying-tree-structures-in-sqlite-using-python-and-the-transitive-closure-extension/
##      or
##      implement MPTT