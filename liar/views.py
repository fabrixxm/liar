import re
import random
import uuid

from flask import (
    flash, g, redirect, render_template, request, session, url_for, abort, make_response
)
from peewee import fn
from playhouse.flask_utils import PaginatedQuery


from .app import app
from .models import User, MailingList, Message, Tag, MessageTag
from . import admin
from . import templatefilters 
from .utils import CustomFontAvatar
from .mailer import send_mail

RESULTS_PER_PAGE=20

@app.before_request
def user_in_session():
    uid = session.get('user')
    g.user = None
    if uid:
        try:
            user = User.get(id = uid)
            g.user = user
        except User.DoesNotExist:
            pass



def get_object_or_404(model, *expressions):
    try:
        return model.get(*expressions)
    except model.DoesNotExist:
        abort(404)


@app.route('/', methods=('GET',))
def index():
    lists = MailingList.select()
    tags = (Tag
            .select(Tag)
            .join(MessageTag)
            .join(Message)
            .group_by(Tag)
            .order_by(fn.Count(Message.id).desc())
            .limit(20)
    )

    threads = Message.get_threads()
    threads = PaginatedQuery(threads, RESULTS_PER_PAGE)
    return render_template('index.html', lists = lists, tags = tags, threads = threads)


@app.route('/r/<string:slug>', methods=('GET',))
def mailinglist(slug):
    mlist = get_object_or_404(MailingList, MailingList.slug == slug)
    threads = mlist.get_threads()

    tags = (Tag
            .select(Tag)
            .join(MessageTag)
            .join(Message)
            .where(Message.mailinglist == mlist)
            .group_by(Tag)
            .order_by(fn.Count(Message.id).desc())
            .limit(20)
    )

    threads = PaginatedQuery(threads, RESULTS_PER_PAGE)
    return render_template('mailinglist.html',  tags = tags,  mailinglist = mlist, threads = threads)


@app.route('/m/<int:id>', methods=('GET',))
@app.route('/m/<int:id>/<string:slug>', methods=('GET',))
def message(id, slug=None):
    if slug is not None:
        message = get_object_or_404(Message, Message.id == id, Message.slug == slug)
    else:
        message = get_object_or_404(Message, Message.id == id)
       
    toplevel = message.get_top_level()
    
    if toplevel != message:
        return redirect(url_for('message', id = toplevel.id, slug = toplevel.slug) + "#c{}".format(message.id) )

    mlist = toplevel.mailinglist


    return render_template('message.html', mailinglist = mlist, message = toplevel)


@app.route('/m/<int:id>/raw', methods=('GET',))
def raw(id):
    message = get_object_or_404(Message, Message.id == id)
    mlist = message.mailinglist
    return render_template('raw.html', mailinglist = mlist, message = message)


@app.route('/s', methods=('GET',))
def search():
    q = request.args.get('q')
    if q is None:
        return redirect(url_for('index'))

    q = q.strip()

    if q.startswith("#"):
        tag = q.strip("#")
        query = Message.select().join(MessageTag, on=(Message.id == MessageTag.message_id)).join(Tag).where( Tag.name == tag)

    else:
        query = Message.ftsearch(q)

    mlist = None
    m = request.args.get('m')
    if m is not None:
        mlist = get_object_or_404(MailingList, MailingList.slug == m)
        query = query.where(Message.mailinglist_id == mlist.id)

    query = query.order_by(Message.parent, -Message.date)
    results = PaginatedQuery(query, RESULTS_PER_PAGE)
    results_count = query.count()
    return render_template('search.html', q = q, mailinglist = mlist, results = results, results_count=results_count)



@app.route('/tags', methods=('GET',))
@app.route('/tags/<string:slug>', methods=('GET',))
def tags(slug = None):
    tags = (Tag
                .select(Tag)
                .join(MessageTag)
                .join(Message)
                .group_by(Tag)
                .order_by(fn.Count(Message.id).desc())
        )
    mlist = None
    if slug is not None:
        mlist = get_object_or_404(MailingList, MailingList.slug == slug)
        tags = tags.where(Message.mailinglist == mlist)

    return render_template('tags.html',  mailinglist = mlist, tags = tags)



@app.route("/u/<int:id>", methods=('GET',))
def user(id):
    user = get_object_or_404(User, User.id == id)
    threads = PaginatedQuery(user.get_threads(), RESULTS_PER_PAGE)
    return render_template('user.html',  user = user, threads = threads)


@app.route("/u/<int:id>/avatar.png", methods=('GET',))
def avatar(id):
    user = get_object_or_404(User, User.id == id)

    size = request.args.get('s') or 128
    if isinstance(size, str):
        if not size.isnumeric():
            size = 128
        else:
            size = int(size)
    if size < 10 or size > 512:
        size = 128

    #return redirect(url_for('static', filename='default-avatar.png'))
    avatar = CustomFontAvatar.generate(size, user.email, "PNG")
    headers = { 'Content-Type': 'image/png' }
    return make_response(avatar, 200, headers)



@app.route("/login", methods=('GET',))
def login():
    n1 = random.randint(1,10)
    n2 = random.randint(1,10)
    op = random.choice(['+', '-', '*'])
    question = "{} {} {}".format(n1, op, n2)
    session['_ca'] = str(eval(question))


    return render_template('login.html',  question = question)

@app.route("/login", methods=('POST',))
def login_post():
    nexturl = request.args.get('next')

    email = request.values.get('email')
    answer = request.values.get('answer')
    ca = session.get('_ca')
    session.pop('_ca', None)

    if ca is None or ca != answer:
        flash("Wrong answer", "danger")
        return redirect(url_for('login', next=nexturl))

    code = str(uuid.uuid4())
    try:
        user = User.get(email = email)

        session['_c'] = code
        session['_u'] = user.id

        emailconf = app.config.get_namespace("EMAIL_")

        url = request.url_root.strip("/") + url_for('login_code', code=code, next=nexturl)
        send_mail(
            emailconf.get('from', 'LiAr'),
            email,
            emailconf.get("subject", "LiAr Login"),
            "Login url:\n\n{}".format(url),
            "<h3>LiAr - Login</h3><p><a href='{0}'>{0}</a></p>".format(url)
        )

    except User.DoesNotExist:
        pass

    return render_template('login_code.html')


@app.route("/login/code/<string:code>", methods=('get',))
def login_code(code):
    nexturl = request.args.get('next')

    sessioncode = session.get('_c')
    uid = session.get('_u')

    if sessioncode is None or uid is None:
        return render_template('login_error.html')

    session.pop('_c', None)
    session.pop('_u', None)

    if sessioncode != code:
        session.pop('user', None)
        return render_template('login_error.html')

    session['user'] = uid

    if nexturl is None or nexturl == "":
        nexturl = url_for('index')

    print(nexturl)

    return redirect(nexturl)


@app.route("/logout", methods=('get',))
def logout():
    session.pop('user', None)
    return redirect(url_for('index'))

