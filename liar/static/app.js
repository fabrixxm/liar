// setup burger menu
document.querySelector('.navbar-burger').addEventListener('click', (e) => {
    e.preventDefault();
    document.getElementById(e.target.dataset.target).classList.toggle('is-active');
});



// setup hidden quotes
document.querySelectorAll("blockquote[id]").forEach( (eQuote) => {
    // if eQuote is inside another blockquote, quit
    console.log("closest", eQuote.parentElement.closest('blockquote') );
    if (eQuote.parentElement.closest('blockquote') !== null) return;

    var eLinkShow = document.querySelector('a.quote-show[data-ref="' + eQuote.id + '"]');
    var eLinkHide = document.querySelector('a.quote-hide[data-ref="' + eQuote.id + '"]');

    // collapse quote if is 'too big' (maginc numbers everywhere)
    if (eQuote.clientHeight > 130) {
        eQuote.classList.add("is-hidden");
        eLinkShow.classList.remove("is-hidden");
    }

    eLinkHide.classList.remove('is-hidden');

    eQuote.addEventListener("click", (e) => {
        e.preventDefault();
        eQuote.classList.add("is-hidden");
        eLinkShow.classList.remove("is-hidden");
    });

    eLinkShow.addEventListener("click", (e) => {
        e.preventDefault();
        eQuote.classList.remove("is-hidden");
        eLinkShow.classList.add("is-hidden");
    });

    eLinkHide.addEventListener( "click" , (e) => {
        e.preventDefault();
        eQuote.classList.add("is-hidden");
        eLinkShow.classList.remove("is-hidden");
    });
});



// timeago
function update_times(){
    if (window['humanized_time_span'] === undefined) {
        setTimeout(update_times, 500);
        return;
    }
    document.querySelectorAll("time").forEach( (eTime) => {
        var dt = eTime.attributes['datetime'].value;
        if (!eTime.classList.contains("raw-datetime")) {
            eTime.innerText = humanized_time_span(dt);
        }
    });
    setTimeout(update_times, 60000);
}

document.querySelectorAll("time").forEach( (eTime) => { 
    eTime.addEventListener('click', (e) => {
        if (window['humanized_time_span'] === undefined) return;
        var dt = eTime.attributes['datetime'].value;
        eTime.classList.toggle("raw-datetime");
        if (eTime.classList.contains("raw-datetime")) {
            eTime.innerText = dt;
        } else {
            eTime.innerText = humanized_time_span(dt);
        }
    });
});
update_times();
