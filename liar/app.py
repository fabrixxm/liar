import os
from flask import Flask

__all__ = ['app']

# create and configure the app
app = Flask(__name__)
app.config.from_mapping(
    SECRET_KEY='dev',
    DATABASE="sqliteext:///liar.sqlite",
    FLASK_ADMIN_SWATCH='cerulean',
)

app.config.from_envvar('LIAR_CONFIG', silent=False)

