from .app import app
from . import views

def create_app(test_config=None):
    return app