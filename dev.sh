#!/bin/bash
export FLASK_APP=liar
export FLASK_ENV=development
export LIAR_CONFIG="$PWD/config.cfg"

if [ "$1" == "liar" ]
then
	shift
	./liar.py $@

else
	flask $@
fi
