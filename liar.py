#!/usr/bin/env python
import sys

cmd = ""
if len(sys.argv) > 1:
    cmd = sys.argv[1]

if cmd == "ingest":
    from liar.commands import ingest as command
elif cmd == "load":
    from liar.commands import load as command
elif cmd == "migrate":
    from liar.commands import migrate as command
else:
    print ("usage: liar [cmd]")
    print ()
    print ("migrate")
    print ("ingest")
    print ("load")
    sys.exit()

command.run()